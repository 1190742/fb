package sample;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BuscarFicheiro {

    public static List<Double> lerFicheiroDoFacebook() {

        try {
            File file = new File("src/sample/Facebook.txt");
            Scanner scanner = new Scanner(file);

            List<Double> ids = new ArrayList<Double>();

            while (scanner.hasNext()) {

                // suposto aparecer --> \"kwEntId\":100003680612074

                String idAVer = scanner.next(".|\\w|\\r");
                if (idAVer.contains("kwEntId\\")) {
                    double id = scanner.nextDouble();
                    ids.add(id);
                }

            }

            return ids;

        } catch (FileNotFoundException exception) {
            System.err.println("FICHEIRO NÃO ENCONTRADO!");
        }

        return null;
    }

}